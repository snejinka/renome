<? //dd($product); ?>
<?php $puthImg = JUri::base() . '/components/com_jshopping/files/img_products/'; ?>
<div class="b-col-1-4 product-item">
    <a href="<?= $product->product_link ?>" class="link-product"></a>
    <img class="product-photo" src="<?= $puthImg . $product->product_name_image; ?>"  width="200" height="200">
    <div class="bold"><?= $product->category->{'name_ru-RU'} ?></div>
    <div class="product-marka"><img src="<?= $product->myAttributes[1]->image ?>" height="12" alt=""><?= $product->myAttributes[1]->value ?> <?= $product->myAttributes[2]->value ?></div>
    <div class="product-descript"><?= $product->myAttributes[7]->value ?></div>
    <div class="b-col-1-2 add-to-cart">
        <div class="price-add-to-cart"><?= number_format($product->product_price, 0, '.', ' ') ?> <i>a</i></div>
        <a href='<?php print $product->buy_link?>' type="submit" class="btn btn-add-to-cart">Подробнее</a>
    </div>
</div>
