<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php
// Баг при импорте - не все товары связываются с категориями
jimport('joomla.html.pagination');
$db = &JFactory::getDbo();
$selectAttr = "SELECT `attr_id`, `value_id`, `name_ru-RU` AS `name_attr`
                FROM #__jshopping_attr_values
                ORDER BY `attr_id`";
$countProd = "SELECT COUNT(distinct prod.product_id) FROM `#__jshopping_products` as prod
                  LEFT JOIN `#__jshopping_products_to_categories` AS pr_cat USING (product_id)
                  LEFT JOIN `#__jshopping_categories` AS cat ON pr_cat.category_id = cat.category_id
                  WHERE prod.product_publish = '1' AND prod.product_id = pr_cat.product_id;";
                //   AND cat.category_publish='1'

$db->setQuery($selectAttr);
$arrayAttr = $db->loadObjectList();
if ($this->filters['0']['searchFilter'] != null) {
    $total = count($this->rows);
    // var_dump($_GET['limitstart']);
    $limitstart = 0;
    JRequest::setVar('limitstart', $limitstart);
    $limit = 24;
} else {
    $db->setQuery($countProd);
    $total = $db->loadResult();
    $limitstart = JRequest::getVar('limitstart', 0);
    $limit = 12;
}
// $test = JRequest::getVar('limitstart');

// var_dump($limitstart);
// var_dump($test);

$countElem = count($arrayAttr);
$arrayMarka = array();
$arrayModel = array();
$arrayDvijok = array();
$arrayKuzov = array();
$arrayMirkir = array();
$arrayOEM = array();
// Пагинация
$pagination = new JPagination($total, $limitstart, $limit);
for ($i=0; $i <= $countElem; $i++) {
    if ($arrayAttr[$i]->attr_id == 1) {
        if ($arrayAttr[$i]->name_attr != '') {
            $arrayMarka[$arrayAttr[$i]->value_id] = $arrayAttr[$i]->name_attr;
        }
    } elseif ($arrayAttr[$i]->attr_id == 2) {
        if ($arrayAttr[$i]->name_attr != '') {
            $arrayModel[$arrayAttr[$i]->value_id] = $arrayAttr[$i]->name_attr;
        }
    } elseif ($arrayAttr[$i]->attr_id == 3) {
        if ($arrayAttr[$i]->name_attr != '') {
            $arrayDvijok[$arrayAttr[$i]->value_id] = $arrayAttr[$i]->name_attr;
        }
    } elseif ($arrayAttr[$i]->attr_id == 4) {
        if ($arrayAttr[$i]->name_attr != '') {
            $arrayKuzov[$arrayAttr[$i]->value_id] = $arrayAttr[$i]->name_attr;
        }
    } elseif ($arrayAttr[$i]->attr_id == 5) {
        if ($arrayAttr[$i]->name_attr != '') {
            $arrayMirkir[$arrayAttr[$i]->value_id] = $arrayAttr[$i]->name_attr;
        }
    } elseif ($arrayAttr[$i]->attr_id == 6) {
        if ($arrayAttr[$i]->name_attr != '') {
            $arrayOEM[$arrayAttr[$i]->value_id] = $arrayAttr[$i]->name_attr;
        }
    }
}
// var_dump($arrayMarka);
?>
    <div class="b-row">
        <p class="title title-catalog">Каталог</p>
    </div>
    <div class="b-row filter-catalog">
        <?
        $document = &JFactory::getDocument();
        $renderer = $document->loadRenderer('modules');
        $options = array('style' => 'xhtml');
        $position = 'filter-products';
        echo $renderer->render($position, $options, null);
        ?>
        <form id="filter-form" action="" method="post" name="jshop_filters">
            <!-- Перебираем свойства -->
            <!-- Марка -->
            <select name="marka" size="1">
                <?php if ($marka = JRequest::getVar('mark')): ?>
                    <option>Марка</option>
                    <option value="<?php echo $arrayMarka[$marka]; ?>" selected="<?php echo $marka; ?>"><?php echo $arrayMarka[$marka]; ?></option>
                <?php else : ?>
                    <option selected="">Марка</option>
                <?php endif; ?>

                <?php foreach ($arrayMarka as $key => $attr): ?>
                    <option value="<?php echo $attr; ?>"><?php echo $attr; ?></option>
                <?php endforeach; ?>
            </select>
            <!-- ***** -->
            <!-- Модель -->
            <select name="model" size="1">
                <option selected="">Модель</option>
                <?php foreach ($arrayModel as $key => $attr): ?>
                    <option value="<?php echo $attr; ?>"><?php echo $attr; ?></option>
                <?php endforeach; ?>
            </select>
            <!-- ***** -->
            <!-- Модель -->
            <select name="dvijok" size="1">
                <option selected="">Двигатель</option>
                <?php foreach ($arrayDvijok as $key => $attr): ?>
                    <option value="<?php echo $attr; ?>"><?php echo $attr; ?></option>
                <?php endforeach; ?>
            </select>
            <!-- ***** -->
            <!-- Модель -->
            <select name="kuzov" size="1">
                <option selected="">Кузов</option>
                <?php foreach ($arrayKuzov as $key => $attr): ?>
                    <option value="<?php echo $attr; ?>"><?php echo $attr; ?></option>
                <?php endforeach; ?>
            </select>
            <!-- ***** -->
            <!-- Модель -->
            <select name="mirkir" size="1">
                <option selected="">Маркировка</option>
                <?php foreach ($arrayMirkir as $key => $attr): ?>
                    <option value="<?php echo $attr; ?>"><?php echo $attr; ?></option>
                <?php endforeach; ?>
            </select>
            <!-- ***** -->
            <!-- Модель -->
            <select name="oem" size="1">
                <option selected="">ОЕМ</option>
                <?php foreach ($arrayOEM as $key => $attr): ?>
                    <option value="<?php echo $attr; ?>"><?php echo $attr; ?></option>
                <?php endforeach; ?>
            </select>
            <!-- ***** -->
            <input type="text" name="comment" placeholder="<?php echo $this->rows[0]->myAttributes[7]->name ?>" class="input-contacts">
            <input type="hidden" name="searchFilter" value="1">
            <button class="btn apply" type="submit">Применить</button>
            <?php if (isset($marka)) : ?>
                <script>
                    // console.log(jQuery('[name = "marka"]').attr('value'));
                    var url = window.location.href;
                    url = url.split('&mark')[0];
                    console.log(url);
                    if (jQuery('[name = "marka"]').attr('value') != 'Марка') {
                        setLocation(url);
                        jQuery('#filter-form').submit();
                        }

                    function setLocation (url) {
                        try {
                          history.pushState(null, null, url);
                          return;
                        } catch(e) {}
                        location.href = url;
                    }
                </script>
            <?php endif; ?>
        </form>
    </div>

    <div class="b-row">
        <?php foreach ($this->rows as $k => $product) : ?>
            <? include(dirname(__FILE__)."/../list_products/product.php"); ?>
        <? endforeach; ?>
    </div>
    <div class="page_counter">
        <?php echo $pagination->getPagesCounter(); ?>
    </div>
    <div class="pagination">
        <?php echo $pagination->getPagesLinks(); ?>
    </div>
