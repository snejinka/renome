<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php
$db = &JFactory::getDbo();
$selectAttr = "SELECT `attr_id`, `name_ru-RU` AS `name_attr`
                FROM #__jshopping_attr_values
                ORDER BY `attr_id`";
$db->setQuery($selectAttr);
$arrayAttr = $db->loadAssocList();
// var_dump($arrayAttr);
?>
    <div class="b-row">
        <p class="title title-catalog">Каталог</p>
    </div>
    <div class="b-row filter-catalog">
        <?
        $document = &JFactory::getDocument();
        $renderer = $document->loadRenderer('modules');
        $options = array('style' => 'xhtml');
        $position = 'filter-products';
        echo $renderer->render($position, $options, null);
        ?>
        <?php
            $countElem = count($this->rows);
            $countAttributes = count($this->rows['0']->myAttributes) - 1;
            //var_dump($this->rows['0']->myAttributes[1]);
        ?>
        <form id="filter-form">
            <!-- Перебираем свойства -->
            <?php for ($i = 1;$i <= $countAttributes; $i++) : ?>
            <select name="select" size="1">
                <option selected=""><?php echo $this->rows[0]->myAttributes[$i]->name ?></option>
                <!-- Собственно сами свойства -->
                <?php for($j = 0; $j < $countElem; $j++) :
                    $attrElem = $this->rows[$j]->myAttributes;
                    ?>
                    <!-- Отбрасываем пустые элементы -->
                    <?php if ($attrElem[$i]->value != '') : ?>
                        <option value="<?php echo $attrElem[$i]->value_id; ?>"><?php echo $attrElem[$i]->value ?></option>
                    <? endif; ?>
                    <!-- *************************** -->
                <?php endfor; ?>
                <!-- *********************** -->
            </select>
            <?php endfor; ?>
            <!-- ****************** -->
            <input type="text" name="comment" placeholder="<?php echo $this->rows[0]->myAttributes[7]->name ?>" class="input-contacts">
            <button class="btn apply" type="submit">Применить</button>
        </form>
    </div>

    <div class="b-row">
        <?php foreach ($this->rows as $k => $product) : ?>
            <? include(dirname(__FILE__)."/../list_products/product.php"); ?>
        <? endforeach; ?>
    </div>
