<?php
/**
* @version      4.10.2 05.11.2013
* @author       MAXXmarketing GmbH
* @package      Jshopping
* @copyright    Copyright (C) 2010 webdesigner-profi.de. All rights reserved.
* @license      GNU/GPL
*/
defined('_JEXEC') or die('Restricted access');
$product = $this->product;
include(dirname(__FILE__)."/load.js.php");
array_shift($this->images);
//dd($product);
?>
<div class="b-row">
    <p class="title"><?= $product->myAttributes[3]->value ?>, <?= $product->myAttributes[1]->value ?></p>
</div>
<div class="b-row wrap-product">
    <form name="product" method="post" action="<?php print $this->action?>" enctype="multipart/form-data" autocomplete="off">
        <div class="b-col-1-2 product-slider">
            <div class="big-slider">
                <a href="<?php print $this->image_product_path?>/<?= $product->image ?>" class="lightbox"><img src="<?= $this->image_product_path ?>/<?= $product->image ?>" height="327" width="327"></a>
            </div>
            <div class="small-slider">
                <? foreach ($this->images as $image): ?>
                    <div class="small-slider-item"><a href="<?= $this->image_product_path ?>/<?= $image->image_name ?>" class="lightbox"><img src="<?= $this->image_product_path ?>/<?= $image->image_name ?>" height="105" width="105"alt=""></a></div>
                <? endforeach; ?>
            </div>
        </div>

        <div class="b-col-1-2">
            <table class="title-list">
                <? foreach ($product->myAttributes as $attr): ?>
                    <tr class="tr-title">
                        <td class="title-list-left" width="155"><?= $attr->name ?>:</td>
                        <td class="title-list-right">
                            <? if ($attr->image): ?>
                                <img src="<?= $attr->image ?>" height="8" alt="">
                            <? endif; ?>
                            <span><?= $attr->value ?></span>
                            <input type="hidden" name="jshop_attr_id[<?= $attr->id ?>]" value="<?= $attr->value_id ?>">
                        </td>
                    </tr>
                <? endforeach; ?>
            </table>
        </div>
        <div id="price-reserv" class="b-col-1-2">
            <p class="price"><?= number_format($product->product_price, 0, '.', ' ') ?> <i>a</i></p>
        </div>
        <button class="btn reserv" type="submit" onclick="jQuery('#to').val('cart');">Забронировать</button>

        <input type="hidden" name="quantity" id="quantity" onkeyup="reloadPrices();" class="inputbox" value="<?php print $this->default_count_product?>" />
        <input type="hidden" name="to" id='to' value="cart" />
        <input type="hidden" name="product_id" id="product_id" value="<?php print $this->product->product_id?>" />
        <input type="hidden" name="category_id" id="category_id" value="<?php print $this->category_id?>" />
    </form>
</div>
