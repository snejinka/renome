<?php
/**
* @version      4.8.0 13.08.2013
* @author       MAXXmarketing GmbH
* @package      Jshopping
* @copyright    Copyright (C) 2010 webdesigner-profi.de. All rights reserved.
* @license      GNU/GPL
*/
defined('_JEXEC') or die('Restricted access');
?>
<?php if ($this->display_list_products){?>
<!-- <div class="jshop_list_product">     -->
<?php
    // var_dump($this);
    if (count($this->rows)){
        foreach ($this->rows as $k => $product) :
            include(dirname(__FILE__)."/../list_products/product.php");
        endforeach;
    } else {
        include(dirname(__FILE__)."/../list_products/no_products.php");
    }
    if ($this->display_pagination){
        include(dirname(__FILE__)."/../list_products/block_pagination.php");
    }
?>
<!-- </div> -->
<?php }?>
