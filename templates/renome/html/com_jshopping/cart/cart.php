<?php
/**
 * @version      4.10.0 22.10.2014
 * @author       MAXXmarketing GmbH
 * @package      Jshopping
 * @copyright    Copyright (C) 2010 webdesigner-profi.de. All rights reserved.
 * @license      GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');

$countprod = count($this->products);
?>
<div class="b-row">
    <p class="title">Корзина</p>
</div>
<div class="b-row">
    <div class="b-col-1 wrp-table-cart">
        <?php if ($countprod > 0) : ?>
            <form action="<?php print SEFLink('index.php?option=com_jshopping&controller=cart&task=refresh') ?>"
                  method="post" name="updateCart">
                <table width="1000" height="260" class="table-cart">
                    <thead>
                    <tr>
                        <td width="775">Наименование</td>
                        <td width="95">Цена</td>
                        <td>Удалить</td>
                    </tr>
                    </thead>
                    <tbody>

                    <? foreach ($this->products as $key_id => $prod): ?>
                        <tr class="item-cart">
                            <td class="title-item-cart"><?= $prod['myAttributes'][1]->value ?> <?= $prod['myAttributes'][2]->value ?></td>
                            <td><?= number_format($prod['price'], 0, '.', ' ') ?> <i>a</i></td>
                            <td class="del-item-cart">
                                <a class="btn btn-del button-img" href="<?php print $prod['href_delete']?>" onclick="return confirm('<?php print _JSHOP_CONFIRM_REMOVE?>')"></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                    <tr class="price-item-cart ">
                        <td>Стоимость товаров</td>
                        <td><?= number_format($this->summ, 0, '.', ' ') ?> <i>a</i></td>
                        <td></td>
                    </tr>
                    <tr class="delivery">
                        <td>Способ доставки: <?= $this->shipping->name ?></td>
                        <td><?= $this->shipping->price ?> <i>a</i></td>
                        <td></td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr class="total-price-cart">
                        <td class="td-total-price">Итого:</td>
                        <td><?= number_format($this->fullsumm, 0, '.', ' ') ?> <i>a</i></td>
                        <td></td>
                    </tr>
                    </tfoot>
                </table>
            </form>
        <? endif; ?>
    </div>
</div>

<div class="b-row">
    <div class="b-col-1">
        <p class="title-form">Оформление заказа</p>

        <p class="title-form-grey">Для того чтобы забронировать товар заполните следующую форму</p>
    </div>

    <div class="b-col-1">
        <form action="<?= JRoute::_('index.php?option=com_jshopping&controller=cart&task=order') ?>" method="post">
            <div class="all-input-info">
                <div class="col input-info">
                    <input required name="company" type="text" class="input-reserv input-company-name"
                           placeholder="Название компании*">
                    <input required name="fio" type="text" class="input-reserv input-fio" placeholder="ФИО*">
                </div>
                <div class="col place-number">
                    <input required name="place" type="text" class="input-reserv-1-2 input-place"
                           placeholder="Город*">
                    <input required name="phone" type="text" class="input-reserv-1-2 input-phone"
                           placeholder="Телефон*">
                </div>
                        <textarea name="info" type="text" class="input-reserv input-dop-info"
                                  placeholder="Дополнительная информация"></textarea>
                <button type="submit" class="btn reserv">Забронировать</button>
            </div>


        </form>
    </div>
</div>