<div class="b-row">
    <div class="b-col-1 big-map">

        <?php
            $document = &JFactory::getDocument();
            $renderer = $document->loadRenderer('modules');
            $options = array('style' => 'xhtml');
            $position = 'yamap';
            echo $renderer->render($position, $options, null);
        ?>

        <p class="p-contacts">Торговая компания Автоснаб осуществляем поставку двигателей и кпп из Японии Европы Америки
            На российском рынке работаем более 5 лет.</p>
    </div>
</div>
<div class="form-order-call">
    <div class="b-col b-col-1-5 b-offset-1-20 addres-contacts">г. Иркутск ул. Трактовая 20/а
        <p id="kars">(Посуда-центр ''Торговый дом карс'')</p>
    </div>
    <div class="b-col b-col-1-5 number-contacts">8 (904) 112-74-79
        <a id="orderCall" href="#">Заказать звонок</a>
    </div>
    <button class="btn btn-connect">Связаться с нами</button>
    <p class="p-contacts-answers">Ответим на все ваши вопросы</p>
    <input required type="text" placeholder="Имя" class="input-contacts">
    <input required type="text" placeholder="Email" class="input-contacts">
    <textarea required type="text" placeholder="Сообщение" class="input-msg-contacts"></textarea>
    <button type="submit" class="btn btn-primary btn-contacts-form">Отправить</button>
</div>