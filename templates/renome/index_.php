<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.renome
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$user = JFactory::getUser();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option = $app->input->getCmd('option', '');
$view = $app->input->getCmd('view', '');
$layout = $app->input->getCmd('layout', '');
$task = $app->input->getCmd('task', '');
$itemid = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');

// Output as HTML5
$doc->setHtml5(true);

if ($task == "edit" || $layout == "form") {
    $fullWidth = 1;
} else {
    $fullWidth = 0;
}

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
//$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/template.js');

// Add Stylesheets
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/app.css');
// Load optional RTL Bootstrap CSS
//JHtml::_('bootstrap.loadCss', false, $this->direction);
$baseUrl = JUri::base();
$isHomePage = $baseUrl === JUri::current();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>"
      lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="cmsmagazine" content="514f04b2a7feed8311452d55eddc6b7b"/>
<!--    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700' rel='stylesheet' type='text/css'>-->
        <link href='assets/fonts/ubuntu/ubuntu.css' rel='stylesheet' type='text/css'>
<!--    <link rel="stylesheet" href="--><?//= $this->baseurl . '/templates/' . $this->template . '/css/app.css' ?><!--">-->
    <link rel="stylesheet" href="assets/css/app.css">
    <jdoc:include type="head"/>

    <!--[if lt IE 9]>
    <script src="<?php echo JUri::root(true); ?>/media/jui/js/html5.js"></script>
    <![endif]-->
    <script src="<?php echo JUri::root(true); ?>/assets/js/script.js"></script>
</head>

<body class="site <?php echo $option
    . ' view-' . $view
    . ($layout ? ' layout-' . $layout : ' no-layout')
    . ($task ? ' task-' . $task : ' no-task')
    . ($itemid ? ' itemid-' . $itemid : '')
    . ($params->get('fluidContainer') ? ' fluid' : '');
echo($this->direction == 'rtl' ? ' rtl' : '');
?>">
<div class="h-page">
    <div id="header" class="b-row">
        <div id="logo" class="b-col-1-4"><a href="<?= JUri::base() ?>"><img src="<?= JUri::base() ?>/assets/img/logo.png" alt="logo"></a></div>
        <div class="b-col-7-10 b-offset-1-20">
            <div class="b-row">
                <div id="contacts" class="b-col b-col-1-5"><span>8 (904) 112-74-79</span>
                    <span class="orderCall">Заказать звонок</span>
                </div>

                <div id="addres" class="b-col b-col-1-5 b-offset-1-20">г. Иркутск ул. Трактовая 20/а
                    <p id="kars">(Посуда-центр ''Торговый дом карс'')</p>
                </div>
                <div id="cart" class="b-col b-col-1-5 b-offset-1-20 wrap-cart">
                    <jdoc:include type="modules" name="header-cart" style="xhtml" />
                </div>
            </div>
            <div class="b-row">
                <div class="b-col-7-10">
                    <jdoc:include type="modules" name="search" style="xhtml" />
                </div>
            </div>
        </div>

    </div>

    <div class="nav nav-top-menu b-row">
        <div class="b-col-1">
            <jdoc:include type="modules" name="top-menu" style="xhtml" />
        </div>
    </div>

</div>

<?php if ($this->countModules('banner-main')) : ?>
    <div class="wrp-slider">
        <div class="h-page">
            <div class="b-row">
                <div class="b-col-1">
                    <a class="catalog" href="/index.php?option=com_jshopping&view=products">В каталог</a>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="h-page b-wrp-contacts-page">
    <jdoc:include type="message" />
    <?php if(!$isHomePage) : ?>
        <jdoc:include type="modules" name="breadcrumbs" style="xhtml" />
    <?php endif; ?>
    <jdoc:include type="component" />
</div>

<div class="h-page">
    <?php if ($this->countModules('banner-action')) : ?>
    <div class="b-row h-banners">
        <div id="sale5" class="b-col-1-2 b-banner-item b-banner-left"><a href="#"><img src="assets/img/action1.png" alt=""></a></div>
        <div id="sale3" class="b-col-1-2 b-banner-item b-banner-right"><a href="#"><img src="assets/img/action2.png" alt=""></a></div>
    </div>
    <?php endif; ?>

    <jdoc:include type="modules" name="search_auto_by_mark" style="xhtml" />

    <?php if ($this->countModules('catalog-main')) : ?>
        <div class="b-row">
            <div id="catalogText" class="b-col-1">Каталог</div>
            <div id="engine" class="b-col-3-10"><a href="#"><img src="assets/img/engine.png" alt=""></a></div>
            <div id="transmission" class="b-col-3-10 b-offset-1-20"><a href="#"><img src="assets/img/transmission.png" alt=""></a></div>
            <div id="allCatalog" class="b-col-3-10 b-offset-1-20"><a href="catalog.php"><img src="assets/img/allCatalog.png" alt=""></a></div>
        </div>
    <? endif; ?>

    <?php if ($this->countModules('catalog-main')) : ?>
        <div class="b-row">
        <div id="Company" class="b-col-1">О компании</div>
        <div class="b-col-1">
            <div id="contentImg"></div>
            <p id="companyText">
                Интернет-магазин Exist.ru начал свою работу в 1999 году. С самого начала главной целью было предложить
                нашим клиентам самый широкий спектр автомобильных запасных частей и аксессуаров, а развитие
                интернет–технологий дало возможность максимально упростить и ускорить процесс покупки.<br>
                Компания быстро росла, и сегодня, занимая одну из ведущих позиции на этом рынке, мы не стоим на месте. В
                основе проекта Exist.ru: самые современные информационные технологии, собственные программные
                разработки, накопленная за годы работы аналитическая и статистическая информация по рынку,
                высококвалифицированный коллектив — мы делаем все для того, чтобы Вы были довольны нашей работой.<br>
                Сегодня мы предлагаем нашим клиентам:<br>
                Обширный on-line каталог автозапчастей для автомобилей европейских, японских и корейских производителей.<br>
                Возможность поиска интересующих деталей различными способами: запрос по VIN автомобиля, номеру запчасти,
                иллюстрированному каталогу.<br>
                Получение максимально полной информации о детали — наличие аналогов у различных производителей,
                применяемости, ценах и сроках поставки.<br>
                В наших прайс-листах более 26 млн наименований оригинальных и неоригинальных зап.частей и аксессуаров от
                1200 ведущих мировых производителей.<br>
                Мы первыми на российском рынке представляем продукцию компаний AJUSA, NK, Mapco, Meyle...<br>
                Мы сотрудничаем более чем с 300-ми поставщиками из Европы, ОАЭ, Японии и Белоруссии<br>
                Запчасти для тюнинга и спецавтомобилей.<br>
                Каждый клиент получает персонального менеджера, который отслеживает всю цепочку от размещения заказа до
                его получения.<br>
                >Полностью автоматизированная система клиентского on-line контроля за выполнением заказа: компьютерное
                управление оплатой/доставкой/отгрузкой заказа Оповещения любым удобным способом: SMS/E-mail/через
                интернет-магазин<br>
                Быстрый документооборот и любая удобная форма оплаты.
            </p>
        </div>
    </div>
    <? endif; ?>
</div>

<div id="footer" class="h-page">
    <div class="b-row">
        <div id="contacts" class="b-col-1-4">8 (904) 112-74-79
            <a class="orderCall" href="#">Заказать звонок</a>
        </div>
        <div id="addres" class="b-col-1-4">г. Иркутск ул. Трактовая 20/а
            <p id="kars">(Посуда-центр ''Торговый дом карс'')</p>
        </div>
        <div  id="avtor" class="b-col-1-4">Создание сайта: ITB-company
            <a href="//itb-company.com" class="copyright">&copy; Copyright 2015</a>
        </div>
    </div>
</div>
<div class="md-modal" id="modal-1">
    <div class="md-head">
        <h3>Заказать звонок</h3>
        <button class="close-modal">&times;</button>
    </div>
    <div class="md-content">
        <jdoc:include type="modules" name="modal_window" style="none"/>
    </div>
    <div class="md-footer">
        <img src="<?php echo JUri::root(true); ?>/images/img-modal-window.png" alt="Движок">
    </div>
</div>
<div class="md-overlay"></div>
<jdoc:include type="modules" name="debug" style="none"/>
</body>
</html>
