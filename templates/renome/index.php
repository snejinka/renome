<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.renome
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->direction = $doc->direction;
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/template.js');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/app.css');
$baseUrl = JUri::base();
$isHomePage = $baseUrl === JUri::current();
$this->_scripts = array();
$this->_script = array();
$this->_links = array();
$this->_styleSheets = array();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <jdoc:include type="head" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:image" content="/assets/img/logo.png">
    <meta name="robots" content="index, follow">
    <meta name="author" content="itb-company">
    <meta name="cmsmagazine" content="">
    <meta name="yandex-verification" content="">
    <meta name="google-site-verification" content="">
<!--    <link rel="apple-touch-icon" href="apple-touch-icon.png">-->
    <link rel="stylesheet" href="/assets/fonts/intro/intro.css">
    <link rel="stylesheet" href="/assets/fonts/opensans/opensans.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/vendors.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/app.min.css"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
</head>
<body>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<header>
    <div class="h-page wrp-head">
        <div class="b-row">
            <div class="b-col-1-2">
                <a href="<?php echo JURI::base(); ?>">
                    <img class="logo" src="/assets/img/logo.png" width="122" height="80" alt=""/>
                </a>
<!--                <div class="choose-lang">-->
<!--                    <ul>-->
<!--                        <li><a href="#">Рус</a></li>-->
<!--                        <li><a href="#">Eng</a></li>-->
<!--                        <li><a href="#">Каз</a></li>-->
<!--                    </ul>-->
<!--                </div>-->
            </div>
            <div class="b-col-1-2">
                <div class="b-col-1-4">
                    <div class="addres">
                        <div>
                            Республика Казахстан, г. Алматы
                            пр. Жибек Жолы, д.50, оф. 100
                        </div>
                        <a href="../assets/img/price.jpg" download class="btn btn-primary btn-black">Скачать прайс</a>
                    </div>
                </div>
                <div class="b-col-1-4">
                    <div class="phone">
                        +7 727 <b>273-55-52</b>
                        +7 727 <b>273-52-20</b>
                        <a class="btn btn-primary btn-black btn-call">Заказать звонок</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="overlay"></div>
        <div class="wrp-form">
            <button class="btn-close"></button>
            <jdoc:include type="modules" name="formcall" style="xhtml" />
        </div>
    </div>
    <jdoc:include type="modules" name="primarynavigation" style="xhtml" />
</header>
<?php if($isHomePage) : ?>
    <jdoc:include type="modules" name="mainslider" style="xhtml" />
    <div class="h-page">
        <jdoc:include type="modules" name="catalogcategory" style="xhtml" />
        <jdoc:include type="modules" name="news" style="xhtml" />
        <section class="wrp-about">
            <?php if($isHomePage) :?>
                <div class="b-row">
                    <div class="b-co-1">
                        <a href="<?php echo JURI::base() . "o-kompanii"; ?>">
                            <h2 class="title">О компании</h2>
                        </a>
                    </div>
                </div>
            <?php endif ;?>
            <jdoc:include type="modules" name="okompanii" style="xhtml" />
        </section>


    </div>
<?php endif; ?>
    <jdoc:include type="component" />
<footer>

    <jdoc:include type="modules" name="primarynavigation" style="xhtml" />
    <div class="h-page wrp-foot">
        <div class="b-row">
            <div class="b-col-1-4">
                <div class="addres">
                    <div>
                        Республика Казахстан, г. Алматы
                        пр. Жибек Жолы, д.50, оф. 100
                    </div>
                    <a href="../assets/img/price.jpg" download class="btn btn-primary btn-white">Скачать прайс</a>
                </div>
            </div>
            <div class="b-col-1-4">
                <div class="phone">
                    +7 727 <b>273-55-52</b>
                    +7 727 <b>273-52-20</b>
                    <a class="btn btn-primary btn-white btn-call">Заказать звонок</a>
                </div>
            </div>
            <div class="b-col-1-4 copy">
                <div>Создание и продвижение сайта: <a href="http://itb-company.com/" target="_blank">ITB-company</a></div>
                <div>&laquo;Бизнес Реноме&raquo;и &laquo;Ак Берен Бк&raquo;</div>
                <div>&copy;2015</div>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="/assets/js/vendors.min.js"></script>
<script type="text/javascript" src="/assets/js/main.min.js"></script>
</body>
</html>