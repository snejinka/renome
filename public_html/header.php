<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta property="og:image" content="/assets/img/logo.png">
	<meta name="robots" content="index, follow">
	<meta name="keywords" content="">
	<meta name="title" content="">
	<meta name="author" content="itb-company">
	<meta name="description" content="">
	<meta name="cmsmagazine" content="">
	<meta name="yandex-verification" content="">
	<meta name="google-site-verification" content="">
<!--	<link rel="apple-touch-icon" href="apple-touch-icon.png">-->
	<link rel="stylesheet" href="/assets/css/app.css">
	<link rel="stylesheet" href="/assets/fonts/intro/intro.css">
	<link rel="stylesheet" href="/assets/fonts/opensans/opensans.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/vendors.css"/>
	<link rel="stylesheet" type="text/css" href="/assets/css/app.min.css"/>


</head>
<body>
	<header>
		<div class="h-page wrp-head">
			<div class="b-row">
				<div class="b-col-1-2">
					<a href="index.php">
						<img class="logo" src="/assets/img/logo.png" width="122" height="80" alt=""/>
					</a>
					<div class="choose-lang">
						<ul>
							<li><a href="#">Рус</a></li>
							<li><a href="#">Eng</a></li>
							<li><a href="#">Каз</a></li>
						</ul>
					</div>
				</div>
				<div class="b-col-1-2">
					<div class="b-col-1-4">
						<div class="addres">
						<div>
							Республика Казахстан, г. Алматы
							пр. Жибек жолы, д.50, оф. 100
						</div>
							<a href="#" download class="btn btn-primary btn-black">Скачать прайс</a>
						</div>
					</div>
					<div class="b-col-1-4">
						<div class="phone">
							+7 727 <b>273-55-52</b>
							+7 727 <b>273-52-20</b>
							<button class="btn btn-primary btn-black">Заказать звонок</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<? include 'primary-navigation.php' ?>
	</header> 

