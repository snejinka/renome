
<footer>
    <? include 'primary-navigation.php' ?>
    <div class="h-page wrp-foot">
        <div class="b-row">
                <div class="b-col-1-4">
                    <div class="addres">
						<div>
							Республика Казахстан, г. Алматы
							пр. Жибек жолы, д.50, оф. 100
						</div>
                        <a href="#" download class="btn btn-primary btn-white">Скачать прайс</a>
                    </div>
                </div>
                <div class="b-col-1-4">
                    <div class="phone">
							+7 727 <b>273-55-52</b>
							+7 727 <b>273-52-20</b>
                        <button class="btn btn-primary btn-white">Заказать звонок</button>
                    </div>
                </div>
                <div class="b-col-1-4 copy">
                        <div>Создание и продвижение сайта: <a href="http://itb-company.com/" target="_blank">ITB-company</a></div>
                        <div>&laquo;Бизнес Реноме&raquo;и &laquo;Ак Берен Бк&raquo;</div>
                        <div>&copy;2015</div>
                </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="/assets/js/vendors.min.js"></script>
<script type="text/javascript" src="/assets/js/main.min.js"></script>
</body>
</html> 