<section class="wrp-news">
    <div class="b-row">
        <div class="b-co-1">
            <h2>Наши новости</h2>
        </div>
    </div>
    <div class="b-row wrp-news-item">
        <video class="b-col-1-2" width="505" height="200" controls="controls" poster="/assets/img/video-poster1.png">
            <source src="URL">
        </video>
        <div class="b-col-1-2 news-caption">
            <a href="#">Портреты из гофрированного картона: необычные работы от Giles Oldershaw</a>
            <p>Английский художник Giles Oldershaw обладает необычным талантом</p>
        </div>
    </div>
    <div class="b-row wrp-news-item">
        <video class="b-col-1-2" width="505" height="200" controls="controls" poster="/assets/img/video-poster2.png">
            <source src="URL">
        </video>
        <div class="b-col-1-2 news-caption">
            <a href="#">Световая инсталляция &laquo;Бумажная люстра&raquo;</a>
            <p>Архитектурная студия Cristina Parreño Architecture создала необычную световую установку</p>
        </div>
    </div>
</section>
