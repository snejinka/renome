$(document).ready(function () {

    $('.main-slider').slick({
        autoplay: false,
        autoplaySpeed: 5000,
        arrows: true,
        centerMode: true,
        centerPadding: '0',
        variableWidth: true,
        prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"> </button>',
        nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"> </button>',
        dots: true
    });
    $('.slick-dots li button').remove();
    //$('.slick-arrow').wrap('<div class="wrp-btn"></div>');
});