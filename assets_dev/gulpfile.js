/**
 * Created by ITB-Oleg on 15.12.2014.
 */
/*global require*/

var WORK_OUT_FOLDER = '../assets/',
    PROD = false;

var gulp        = require('gulp'),
    plumber     = require('gulp-plumber'),
    size        = require('gulp-filesize'),

    spritesmith = require('gulp.spritesmith'),
    imagemin    = require('gulp-imagemin'),
    pngquant    = require('imagemin-pngquant'),
	buffer      = require('vinyl-buffer'),
    merge       = require('merge-stream'),

    stylus      = require('gulp-stylus'),
    autoprefixer = require('gulp-autoprefixer'),
    minifyCSS   = require('gulp-minify-css'),

    babel       = require('gulp-babel'),
    concat      = require('gulp-concat'),
    rename      = require('gulp-rename'),
    uglify      = require('gulp-uglify');

gulp.task('js', function () {
    "use strict";

    return gulp.src(['js/**/*.js'])
        .pipe(plumber())
        .pipe(babel())
        .pipe(concat('main.js'))
        .pipe(gulp.dest(WORK_OUT_FOLDER + 'js/'))
        .pipe(rename('main.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(WORK_OUT_FOLDER + 'js'))
        .pipe(size());
});


gulp.task('vendors', function () {
    "use strict";

    gulp.src([
        //'./vendor/nouislider/nouislider.css',
        //'./vendor/nouislider/nouislider.pips.css',
        //'./vendor/chosen/chosen.min.css',
        //'./vendor/icheck/skins/minimal/red.css',
        './vendor/slick/slick.css'
    ])
    .pipe(concat('vendors.css'))
    // .pipe(rename('vendors.min.css'))
    .pipe(minifyCSS())
    .pipe(gulp.dest(WORK_OUT_FOLDER + 'css'));

    var vendors_js = [
            //'./vendor/jquery.min.js',
            //'./vendor/icheck/icheck.min.js',
            //'./vendor/nouislider/nouislider.js',
            //'./vendor/chosen/chosen.jquery.min.js',
            './vendor/slick/slick.min.js'
    ];

    gulp.src(vendors_js)
        .pipe(concat('vendors.js'))
        .pipe(gulp.dest(WORK_OUT_FOLDER + 'js'))
        .pipe(rename('vendors.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(WORK_OUT_FOLDER + 'js'));

});


gulp.task('sprite', function () {
    "use strict";

    var spriteData = gulp.src('img/*.png')
        .pipe(plumber())
        .pipe(spritesmith({
            imgName: '../img/sprite.png',
            cssName: 'sprite.styl'
        }));

    var imgStream = spriteData.img
        .pipe(buffer())
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(WORK_OUT_FOLDER + 'img/'));


    var cssStream = spriteData.css
        .pipe(gulp.dest('styl/'));

    return merge(imgStream, cssStream);

});


gulp.task('stylus-to-css', function () {
    "use strict";

    var stream = gulp.src('styl/app.styl')
        .pipe(plumber())
        .pipe(stylus({
            'include css': true
        }))
        .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
        .pipe(gulp.dest(WORK_OUT_FOLDER + 'css'))
        .pipe(rename({suffix: ".min"}))
        .pipe(minifyCSS())
        .pipe(gulp.dest(WORK_OUT_FOLDER + 'css'));
});



gulp.task('watch', function () {
    "use strict";

    gulp.watch('styl/**/*.styl', ['stylus-to-css']);
    gulp.watch('js/**/*.js', ['js']);


});

gulp.task('default', ['watch']);