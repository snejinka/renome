$(document).ready(function(){

    $('.main-slider').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: true,
        centerMode: true,
        centerPadding: '0',
        variableWidth: true,
        prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"> </button>',
        nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"> </button>',
        dots: true
    });
    $('.slick-dots li button').remove();
    //$('.slick-arrow').wrap('<div class="wrp-btn"></div>');

    $('body').on('click', '.btn-close, .overlay', function(e){
        $('.wrp-form, .overlay').fadeOut();
    });

    $('body').on('click','.btn-call', function (e) {
        $('.wrp-form, .overlay').fadeIn();
    });


});