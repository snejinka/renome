<?php
/**
 * @copyright	Copyright (c) 2015 mainslider. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;


?>
<div class="wrp-main-slider">
    <div class="main-slider">
        <div><img src="/assets/img/main-slider1.png" width="1000" height="500" alt=""/><div class="white slider-content clearfix"><h1>Гофрокартон</h1><p>Данный упаковочный материал широко применяется в промышленности, дешевый и легкий, но, тем не менее, прочный.</p><a class="btn btn-circle" href="<?php echo JURI::base() ."produktsiya"; ?>">в каталог &emsp;&rarr;</a></div></div>
        <div><img src="/assets/img/main-slider2.png" width="1000" height="500" alt=""/><div class="black slider-content clearfix"><h1>Картон немелованный</h1><p>Данный упаковочный материал широко применяется в промышленности, дешевый и легкий, но, тем не менее, прочный.</p><a class="btn btn-circle" href="<?php echo JURI::base() ."produktsiya"; ?>">в каталог &emsp;&rarr;</a></div></div>
        <div><img src="/assets/img/main-slider2.png" width="1000" height="500" alt=""/><div class="black slider-content clearfix"><h1>Бумага офсетная</h1><p>Данный упаковочный материал широко применяется в промышленности, дешевый и легкий, но, тем не менее, прочный.</p><a class="btn btn-circle" href="<?php echo JURI::base() ."produktsiya"; ?>">в каталог &emsp;&rarr;</a></div></div>
    </div>
</div>