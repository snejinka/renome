<?php
/**
 * @copyright	Copyright (c) 2015 category. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;


?>
<div class="h-page wrp-category">
    <div class="b-row title">
        <div class="b-col-1-2">
                <h3>Бумага</h3>
        </div>
        <div class="b-col-1-2 breadcrumbs">
            <ul>
                <li><a href="<?php echo JURI::base() ; ?>">Главная</a></li>&gt;
                <li><a href="<?php echo JURI::base() ."produktsiya"; ?>">Каталог продукции</a>&gt;</li>
                <li><span>Бумага</span></li>
            </ul>
        </div>
    </div>
    <div class="b-row">
        <?php for($i=0;$i<8;$i++) : ?>
            <jdoc:include type="modules" name="subcategorycard" style="xhtml" />
        <?php endfor ;?>
    </div>
</div>