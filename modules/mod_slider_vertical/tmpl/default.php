<?php
defined('_JEXEC') or die;
$doc = JFactory::getDocument();
$uri = JUri::Base();
 if(!empty($images)) {
?>
    <div class="top-block"></div>
    <div class="wrp-main-slider">
        <div class="main-slider">
            <?php
            foreach($images as $img) {
                if (!empty($img->href)) {
                    echo '<div>';
                    echo '<a href="'.$img->href.'"><img src="'.$uri.$img->src.'" /></a>';
                }
                else {
                    echo '<div>';
                    echo '<img src="'.$uri.$img->src.'" />';
                }
                if(!empty($img->title)) {
//                    var_dump($img->class);
                    echo '<div class="'.$img->class.' slider-content clearfix">';
                    echo '<h1 class="slider_header">'.$img->title.'</h1>';
                    echo '<p class="slider_desc">'.$img->desc.'</p>';
                    echo '<a href="'.$img->href.'" class="btn btn-circle slider_header_mini">'.$img->minititle.'</a>';
                    echo '</div>';
                }
                echo '</div>';
            }
            ?>
        </div>
    </div>

    <div class="bottom-block"></div>
<?php }