<?php

defined('_JEXEC') or die;

jimport('joomla.filesystem.file');

abstract class modSliderVerticalHelper
{

	public static function getItems($params)
	{
        $images = array();
        for( $i = 1 ; $i <= 10 ; $i++ ) {
            $img = new stdClass();
            $src = $params->get('image'.$i);
            $text = $params->get('imagetitle'.$i);
            $desc = $params->get('imagedesc'.$i);
            $href = $params->get('imagehref'.$i);
            $minititle = $params->get('imageminititle'.$i);
            $class = $params->get('imageclass'.$i);



            if(!empty($src)) {
                $img->src = $params->get('image'.$i);
                if(!empty($text)) {
                    $img->title = $text;
                } else {
                    $img->title = '';
                }
                if(!empty($desc)) {
                    $img->desc = $desc;
                } else {
                    $img->desc = '';
                }
                if(!empty($href)) {
                    $img->href = $href;
                } else {
                    $img->href = '';
                }
                if(!empty($minititle)) {
                    $img->minititle = $minititle;
                } else {
                    $img->minititle = '';
                }
                if(!empty($class)) {
                    $img->class = $class;
                } else {
                    $img->class = '';
                }
                                $images[] = $img;
            }

        }

        $repeatList = $params->get('addnew');
        $json = json_decode($repeatList, true);
        foreach ($json['image'] as $key => $value) {
            if (!empty($json['image'][$key])) {
                $img = new stdClass();
                $img->src = $json['image'][$key];
                $img->title = $json['imagetitle'][$key];
                $img->desc = $json['imagedesc'][$key];
                $img->href = $json['imagehref'][$key];
                $img->minititle = $json['imageminititle'][$key];
                $img->class = $json['imageclass'][$key];
                $images[] = $img;
            }
        }

        return $images;
	}

}
