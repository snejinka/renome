<?php
/**
 * @copyright	Copyright (c) 2015 catalog_category. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;


?>
    <div class="b-row wrp-catalog">
        <section class="b-col-1-2">
            <?php if(!$isHomePage) : ?>
                <h2>Бумага</h2>
            <?php endif; ?>
            <a class="link" href="<?php echo JURI::base() ."bumaga"; ?>"><span>в каталог&emsp; &rarr;</span></a>
        </section>
        <section class="b-col-1-2">
            <h2>Картон</h2>
            <a class="link" href="<?php echo JURI::base() ."karton"; ?>"><span>в каталог&emsp; &rarr;</span></a>
        </section>
    </div>

